extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var colourPicker
var colourPickerContainer
var button
var colour
var layout
var penSizeSlider
var alphaSlider
var varianceSlider
var fingersSlider
var saveButton
var loadButton
var loopToggle
var mainNode

# Called when the node enters the scene tree for the first time.
func _ready():
	colourPickerContainer=get_node("GridContainer")
	colourPicker=get_node("GridContainer/ColorPicker")
	penSizeSlider=get_node("GridContainer/VBoxContainer/HSlider")
	alphaSlider=get_node("GridContainer/VBoxContainer/HSlider2")
	varianceSlider=get_node("GridContainer/VBoxContainer/HSlider3")
	button=get_node("GridContainer/VBoxContainer/Button")
	saveButton=get_node("GridContainer/HBoxContainer/SaveButton")
	loadButton=get_node("GridContainer/HBoxContainer/LoadButton")
	loopToggle=get_node("GridContainer/VBoxContainer/LoopCheckBox")
	fingersSlider=get_node("GridContainer/VBoxContainer/HSlider4")
	layout=get_node("GridContainer")
	colour=Color(0,0,0,1)
	button.connect("pressed",Callable(self,"chooseColour"))
	saveButton.connect("pressed", Callable(self,"saveButtonPressed"))
	loadButton.connect("pressed", Callable(self, "loadButtonPressed"))
	mainNode=get_parent()
	var scaleUI= DisplayServer.window_get_size().y/1080.0
	layout.scale = layout.scale*scaleUI
	refresh()
	
func refresh():
	penSizeSlider.value=mainNode.penSize
	alphaSlider.value=mainNode.alpha
	varianceSlider.value=mainNode.variance
	fingersSlider.value=mainNode.fingers
	loopToggle.button_pressed=mainNode.loop
	
func colourPick():
	visible=true
	
func saveButtonPressed():
	mainNode.save()

func loadButtonPressed():
	mainNode.loadDia()
	
func chooseColour():
	colour=colourPicker.get_pick_color()
	print(colour)
	visible=false
	mainNode.colour=colour
	mainNode.penSize=penSizeSlider.value
	mainNode.alpha=alphaSlider.value
	mainNode.variance=varianceSlider.value
	mainNode.fingers=fingersSlider.value
	mainNode.loop=loopToggle.button_pressed
	mainNode.uiActive=false
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
