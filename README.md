# GDVectorPaint



## Getting started

This is a vector paint written using godot 4. I relies on [vectorpaint](https://gitlab.com/dronn/vectorpaint)
that is shared with [vector textured](https://gitlab.com/dronn/vectortextured) that uses vector graphic textures.
So submodules have to be checked out also:

```
git clone --recurse-submodules --remote-submodules https://gitlab.com/dronn/gdvectorpaint.git
```
