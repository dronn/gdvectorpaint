extends "vectorpaint/GDVectorPaint.gd"

func _init(res=null):
	presetResolution=res
	scaleFactor=1.0


func _process(_delta):
	if Input.is_action_just_released("click") and not uiActive and not rerenderHistory:
		rectordStroke=false
		var history=get_node("history")
		var buffer=get_node("SubViewportContainer/SubViewport/Buffer")
		
		buffer.update()
		if(not loop):
			if line!=null:
				line.visible=false
				history.strokes.append(stroke)
				for i in range(line.fingers):
					history.renderedGraphicsObjects.append(line.renderedGraphicObjects[i])
					history.renderedStrokes.append(line.renderedStrokes[i])
				var newGraphicsObject=[1, penSize, fingers, variance, colour, alpha]
				history.graphicsObjects.append(newGraphicsObject)
		else:
			if poly!=null:
				poly.visible=false
				history.strokes.append(stroke)
				var newGraphicsObject=[0, penSize, fingers, variance, colour, alpha]
				#print(newGraphicsObject)
				history.graphicsObjects.append(newGraphicsObject)
				history.renderedGraphicsObjects.append(newGraphicsObject)
				history.renderedStrokes.append(poly.renderedStrokes[0])
		poly=null
		line=null
		stroke=[]
		
	elif Input.is_action_just_pressed("click") and not uiActive:
		rectordStroke=true
	process_redraw()

func savePng(filename):
	var tex = await getTexture()
	var img = tex.get_image()
	if img:
		img.clear_mipmaps()
		var x = img.save_png(filename)


func _input(ev):
	if not uiActive:
		if ev is InputEventKey and ev.keycode == KEY_U and ev.pressed  and not ev.is_echo():
			get_node("history").clearAll()
		if Input.is_action_just_pressed("toggleLoop"):
			loop=not loop
		if (ev is InputEventScreenTouch):
			var touch_index = ev.get_index()
			if(touch_index==1):
				uiActive=true
				get_node("ui").refresh()
				get_node("ui").visible=true
				get_node("ui").colourPick()
		if Input.is_action_just_pressed("colourChange"):
			uiActive=true
			get_node("ui").refresh()
			get_node("ui").visible=true
			get_node("ui").colourPick()

		if Input.is_action_just_pressed("fingerIncrease"):
				fingers=fingers+1
				print("fingers")
				print(fingers)
		if Input.is_action_just_pressed("penIncrease"):
			if(not Input.is_action_just_pressed("fingerIncrease")):
				penSize=penSize+1
		if Input.is_action_just_pressed("fingerDecrease"):
			if(fingers > 1):
				fingers=fingers-1
		if Input.is_action_just_pressed("penDecrease"):
			if not Input.is_action_just_pressed("fingerDecrease"):
				if penSize > 1:
					penSize=penSize-1
		if Input.is_action_just_pressed("randomIncrease"):
					variance=variance+0.1
		if Input.is_action_just_pressed("transparentIncrease"):
			if not Input.is_action_pressed("randomIncrease"):
					if alpha < 1.0:
						alpha =alpha+0.05

		if Input.is_action_just_pressed("randomDecrease"):
					if variance > 0.1:
						variance=variance-0.1
					elif variance < 0.1 && variance > 0.0:
						variance = 0.0
		if Input.is_action_just_pressed("transparentDecrease"):
			if not Input.is_action_pressed("randomDecrease"):
					if alpha > 0.05:
						alpha =alpha-0.05
					elif alpha > 0 && alpha < 0.05:
						alpha =0.0;
		if ev is InputEventKey and ev.keycode == KEY_Q and ev.pressed  and not ev.is_echo():
				get_tree().quit()
		if ev is InputEventKey and ev.keycode == KEY_S and ev.pressed  and not ev.is_echo():
				savePng("/tmp/tst.png")
				uiActive=true
				save()

		if ev is InputEventKey and ev.keycode == KEY_L and ev.pressed  and not ev.is_echo():
				uiActive=true
				loadDia()
		if ev is InputEventMouseMotion and rectordStroke:
			if(not loop):
				if stroke.size()==0:
					stroke.append(ev.position)
					line=get_node("SubViewportContainer/SubViewport/line").duplicate()
					var utility=get_node("utility")
					line.utility=utility
					line.default_color=Color(colour.r, colour.g, colour.b,alpha)
					line.width=penSize
					line.radius=penSize
					line.alpha=alpha
					line.fingers=fingers
					line.variance=variance
					line.begin_cap_mode=Line2D.LINE_CAP_ROUND
					line.end_cap_mode=Line2D.LINE_CAP_ROUND
					if penSize < 1.5:
						line.antialiased=true
					else:
						line.antialiased=false
					#line.add_point(ev.position)
					line.manipulate_line(ev.position)
					var viewport=get_node("SubViewportContainer/SubViewport")
					viewport.add_child(line)
					
				else:
					line.manipulate_line(ev.position)
					stroke.append(ev.position)
			else:
				if stroke.size()==0:
					stroke.append(ev.position)
					poly=get_node("SubViewportContainer/SubViewport/poly").duplicate()
					poly.colour=Color(colour.r, colour.g, colour.b, alpha)
					poly.manipulate_shape(ev.position)
					var viewport=get_node("SubViewportContainer/SubViewport")
					viewport.add_child(poly)
					
					line=Line2D.new()
					line.width=2
					line.add_point(ev.position)
				else:
					stroke.append(ev.position)
					poly.manipulate_shape(ev.position)
					line.add_point(ev.position)
		get_viewport().set_input_as_handled()

