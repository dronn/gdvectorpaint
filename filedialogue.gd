extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func fileSave():
	var mainNode=get_parent().get_parent()
	mainNode.uiActive=true
	$FileDialog.popup()
	
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_FileDialog_file_selected(path):
	var mainNode=get_parent().get_parent()
	mainNode.saveFile(path)
	mainNode.saveBinary(path)
	mainNode.saving=false
	mainNode.uiActive=false
